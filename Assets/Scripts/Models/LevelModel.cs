﻿using MatchGame.Events;
using MatchGame.Levels;
using UnityEngine;

namespace MatchGame.Models
{
    public sealed class LevelModel
    {
        public readonly int Width;
        public readonly int Height;

        private readonly LevelCell[] _grid;

        public LevelModel(int width, int height)
        {
            Width = width;
            Height = height;
            _grid = new LevelCell[Width * Height];
            for (int i = 0; i < _grid.Length; i++)
                _grid[i] = LevelCell.Empty;
        }

        public LevelCell GetCell(int index)
        {
            return _grid[index];
        }

        public void CreateTile(int index, int tileId)
        {
            if (_grid[index] == LevelCell.Empty)
            {
                _grid[index] = new LevelCell(tileId);
                Dispatcher.Dispatch(this, new LevelTileCreatedEvent(index, tileId));
            }
        }

        public void MoveTile(int fromIndex, int toIndex)
        {
            if (_grid[fromIndex] != LevelCell.Empty && _grid[toIndex] == LevelCell.Empty)
            {
                _grid[toIndex] = _grid[fromIndex];
                _grid[fromIndex] = LevelCell.Empty;
                Dispatcher.Dispatch(this, new LevelTileMovedEvent(fromIndex, toIndex));
            }
        }

        public void DestroyTile(int index)
        {
            if (_grid[index] != LevelCell.Empty)
            {
                int tileId = _grid[index].TileId;
                _grid[index] = LevelCell.Empty;
                Dispatcher.Dispatch(this, new LevelTileDestroyedEvent(tileId, index));
            }
        }

        public int PositionToIndex(Vector2Int position)
        {
            return PositionToIndex(position.x, position.y);
        }

        public int PositionToIndex(int x, int y)
        {
            return y * Width + x;
        }

        public Vector2Int IndexToPosition(int index)
        {
            return new Vector2Int(index % Width, index / Width);
        }
    }
}