﻿namespace MatchGame.Pools
{
    public interface IPool
    {
        void Preload(int count);
        object Spawn();
        void Despawn(object instance);
        void Clear();
    }
}