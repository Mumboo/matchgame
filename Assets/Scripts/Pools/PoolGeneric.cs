﻿using System.Collections.Generic;

namespace MatchGame.Pools
{
    public abstract class Pool<T> : IPool<T>
    {
        private readonly Queue<T> _free;

        protected Pool()
        {
            _free = new Queue<T>();
        }

        public void Preload(int count)
        {
            for (int i = 0; i < count; i++)
            {
                T instance = CreateInstance();
                _free.Enqueue(instance);
            }
        }

        public T Spawn()
        {
            T instance;
            if (_free.Count > 0)
                instance = _free.Dequeue();
            else
                instance = CreateInstance();

            OnSpawned(instance);
            return instance;
        }

        public void Despawn(T instance)
        {
            OnDespawned(instance);
            _free.Enqueue(instance);
        }

        public void Clear()
        {
            while (_free.Count > 0)
            {
                T instance = _free.Dequeue();
                DestroyInstance(instance);
            }
        }

        protected abstract T CreateInstance();
        protected abstract void DestroyInstance(T instance);
        protected virtual void OnSpawned(T instance) { }
        protected virtual void OnDespawned(T instance) { }

        object IPool.Spawn()
        {
            return Spawn();
        }

        void IPool.Despawn(object instance)
        {
            Despawn((T)instance);
        }
    }
}