﻿using System.Collections.Generic;
using UnityEngine;

namespace MatchGame.Pools
{
    public sealed class GameObjectPool : Pool<GameObject>
    {
        private static readonly List<IPoolable> _poolableComponents;

        private readonly Transform _parent;
        private readonly GameObject _prefab;

        static GameObjectPool()
        {
            _poolableComponents = new List<IPoolable>(1);
        }

        public GameObjectPool(GameObject prefab)
        {
            _prefab = prefab;
        }

        public GameObjectPool(GameObject prefab, Transform parent) : this(prefab)
        {
            _parent = parent;
        }

        protected override GameObject CreateInstance()
        {
            GameObject instance = Object.Instantiate(_prefab, _parent);
            instance.SetActive(false);
            return instance;
        }

        protected override void DestroyInstance(GameObject instance)
        {
            Object.Destroy(instance);
        }

        protected override void OnSpawned(GameObject instance)
        {
            base.OnSpawned(instance);

            instance.transform.SetParent(null);
            instance.gameObject.SetActive(true);

            instance.GetComponentsInChildren(_poolableComponents);
            for (int i = 0; i < _poolableComponents.Count; i++)
                _poolableComponents[i].OnSpawned();
        }

        protected override void OnDespawned(GameObject instance)
        {
            base.OnDespawned(instance);

            instance.GetComponentsInChildren(_poolableComponents);
            for (int i = 0; i < _poolableComponents.Count; i++)
                _poolableComponents[i].OnDespawned();

            instance.SetActive(false);
            instance.transform.SetParent(_parent);
        }
    }
}