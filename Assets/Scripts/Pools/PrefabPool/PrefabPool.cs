﻿using System.Collections.Generic;
using UnityEngine;

namespace MatchGame.Pools
{
    public sealed class PrefabPool : MonoBehaviour
    {
        private readonly Dictionary<GameObject, GameObjectPool> _pools;
        private readonly Dictionary<GameObject, GameObjectPool> _used;

        public PrefabPool()
        {
            _pools = new Dictionary<GameObject, GameObjectPool>();
            _used = new Dictionary<GameObject, GameObjectPool>();
        }

        public void Preload(GameObject prefab, int count)
        {
            GameObjectPool pool = GetPool(prefab);
            pool.Preload(count);
        }

        public GameObject Spawn(GameObject prefab)
        {
            GameObjectPool pool = GetPool(prefab);
            GameObject instance = pool.Spawn();
            _used.Add(instance, pool);
            return instance;
        }

        public GameObject Spawn(GameObject prefab, Transform parent)
        {
            GameObject instance = Spawn(prefab);
            instance.transform.SetParent(parent);
            return instance;
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            GameObject instance = Spawn(prefab);
            instance.transform.SetPositionAndRotation(position, rotation);
            return instance;
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            GameObject instance = Spawn(prefab, position, rotation);
            instance.transform.SetParent(parent);
            return instance;
        }

        public void Despawn(GameObject instance)
        {
            GameObjectPool pool;
            if (_used.TryGetValue(instance, out pool))
            {
                pool.Despawn(instance);
                _used.Remove(instance);
            }
            else
            {
                Debug.LogWarningFormat("{0} is not spawned from the pool, object will be destroyed", instance.name);
                Object.Destroy(instance);
            }
        }

        public void Clear()
        {
            foreach (GameObjectPool pool in _pools.Values)
                pool.Clear();
            _used.Clear();
        }

        private GameObjectPool GetPool(GameObject prefab)
        {
            GameObjectPool pool;
            if (!_pools.TryGetValue(prefab, out pool))
            {
                GameObject poolParentObject = new GameObject(string.Format("{0} Pool", prefab.name));
                Transform poolParent = poolParentObject.transform;
                poolParent.SetParent(transform);
                pool = new GameObjectPool(prefab, poolParent);
                _pools.Add(prefab, pool);
            }
            return pool;
        }
    }
}