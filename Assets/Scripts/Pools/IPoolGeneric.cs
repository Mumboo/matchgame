﻿namespace MatchGame.Pools
{
    public interface IPool<T> : IPool
    {
        new T Spawn();
        void Despawn(T instance);
    }
}