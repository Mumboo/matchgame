﻿using System;

namespace MatchGame.Levels
{
    public struct LevelCell : IEquatable<LevelCell>
    {
        public static readonly LevelCell Empty = new LevelCell(-1);

        public readonly int TileId;

        public LevelCell(int tileId)
        {
            TileId = tileId;
        }

        public bool Equals(LevelCell other)
        {
            return TileId == other.TileId;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is LevelCell)
                return Equals((LevelCell)obj);
            return false;
        }

        public override int GetHashCode()
        {
            return 700009900 + TileId.GetHashCode();
        }

        public static bool operator ==(LevelCell left, LevelCell right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(LevelCell left, LevelCell right)
        {
            return !left.Equals(right);
        }
    }
}
