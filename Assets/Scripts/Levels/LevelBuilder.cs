﻿using MatchGame.Models;
using Random = System.Random;

namespace MatchGame.Levels
{
    public sealed class LevelBuilder
    {
        private readonly LevelModel _model;
        private readonly Tileset _tileset;
        private readonly Random _random;

        public LevelBuilder(LevelModel model, Tileset tileset, int seed)
        {
            _model = model;
            _tileset = tileset;
            _random = new Random(seed);
        }

        public void Build()
        {
            for (int y = 0; y < _model.Height; y++)
            {
                for (int x = 0; x < _model.Width; x++)
                {
                    int index = _model.PositionToIndex(x, y);
                    LevelCell cell = _model.GetCell(index);
                    if (cell == LevelCell.Empty)
                        _model.CreateTile(index, _random.Next(_tileset.Count));
                }
            }
        }
    }
}
