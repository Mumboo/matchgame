﻿using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Levels
{
    [CreateAssetMenu(menuName = "Levels/Horizontal Line Destroy Tile")]
    public sealed class HorizontalLineDestroyTilesetTile : TilesetTile
    {
        [SerializeField]
        private int _size = 1;

        private void OnValidate()
        {
            _size = Mathf.Max(1, _size);
        }

        public override void Open(LevelModel model, Vector2Int position)
        {
            int minX = Mathf.Max(position.x - _size, 0);
            int maxX = Mathf.Min(position.x + _size, model.Width - 1);
            for (int x = minX; x <= maxX; x++)
            {
                int cellIndex = model.PositionToIndex(x, position.y);
                model.DestroyTile(cellIndex);
            }
        }
    }
}
