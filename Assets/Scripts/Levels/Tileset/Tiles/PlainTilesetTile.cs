﻿using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Levels
{
    [CreateAssetMenu(menuName = "Levels/Tile")]
    public sealed class PlainTilesetTile : TilesetTile
    {
        public override void Open(LevelModel model, Vector2Int position) { }
    }
}
