﻿using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Levels
{
    public abstract class TilesetTile : ScriptableObject
    {
        [SerializeField]
        private Sprite _image;
        public Sprite Image { get { return _image; } }

        [SerializeField]
        private GameObject _createEffect;
        public GameObject CreateEffect { get { return _createEffect; } }

        [SerializeField]
        private GameObject _destroyEffect;
        public GameObject DestroyEffect { get { return _destroyEffect; } }

        [SerializeField]
        private AudioClip _createAudioClip;
        public AudioClip CreateAudioClip { get { return _createAudioClip; } }

        [SerializeField]
        private AudioClip _destroyAudioClip;
        public AudioClip DestroyAudioClip { get { return _destroyAudioClip; } }

        [SerializeField]
        private AudioClip _clickAudioClip;
        public AudioClip ClickAudioClip { get { return _clickAudioClip; } }

        public abstract void Open(LevelModel model, Vector2Int position);
    }
}
