﻿using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Levels
{
    [CreateAssetMenu(menuName = "Levels/Vertical Line Destroy Tile")]
    public sealed class VerticalLineDestroyTilesetTile : TilesetTile
    {
        [SerializeField]
        private int _size = 1;

        private void OnValidate()
        {
            _size = Mathf.Max(1, _size);
        }

        public override void Open(LevelModel model, Vector2Int position)
        {
            int minY = Mathf.Max(position.y - _size, 0);
            int maxY = Mathf.Min(position.y + _size, model.Height - 1);
            for (int y = minY; y <= maxY; y++)
            {
                int cellIndex = model.PositionToIndex(position.x, y);
                model.DestroyTile(cellIndex);
            }
        }
    }
}
