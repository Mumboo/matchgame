﻿using UnityEngine;

namespace MatchGame.Levels
{
    [CreateAssetMenu(menuName = "Levels/Tileset")]
    public sealed class Tileset : ScriptableObject
    {
        public int Count { get { return _tiles.Length; } }

        [SerializeField]
        private TilesetTile[] _tiles;
        [SerializeField]
        private TilesetTile[] _rewards;

        public TilesetTile GetTile(int id)
        {
            if (id >= 0)
            {
                if (id < _tiles.Length)
                    return _tiles[id];
                else if (id < _tiles.Length + _rewards.Length)
                    return _rewards[id - _tiles.Length];
            }
            return null;
        }

        public int GetRewardTileId<T>()
        {
            for (int i = 0; i < _rewards.Length; i++)
            {
                if (_rewards[i].GetType() == typeof(T))
                    return i + _tiles.Length;
            }
            return -1;
        }
    }
}
