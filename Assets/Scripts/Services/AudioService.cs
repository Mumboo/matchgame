﻿using UnityEngine;

namespace MatchGame.Services
{
    public sealed class AudioService : MonoBehaviour
    {
        public bool MusicMute
        {
            get { return _musicSource.mute; }
            set { _musicSource.mute = value; }
        }

        public bool SoundMute
        {
            get { return _soundSource.mute; }
            set { _soundSource.mute = value; }
        }

        [SerializeField]
        private AudioSource _musicSource;

        [SerializeField]
        private AudioSource _soundSource;

        public void PlayMusic(AudioClip clip)
        {
            _musicSource.PlayOneShot(clip);
        }

        public void PlaySound(AudioClip clip)
        {
            _soundSource.PlayOneShot(clip);
        }
    }
}