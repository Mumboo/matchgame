﻿using MatchGame.Events;
using UnityEngine;

namespace MatchGame.Services
{
    public sealed class Storage
    {
        private StorageData _data;
        public StorageData Data
        {
            get { return _data; }
            set
            {
                _data = value;
                Dispatcher.Dispatch(this, new StorageDataChangedEvent(value));
            }
        }

        public Storage()
        {
            Load();
        }

        public void Load()
        {
            _data.CurrentLevel = PlayerPrefs.GetInt("CurrentLevel", _data.CurrentLevel);
        }

        public void Save()
        {
            PlayerPrefs.SetInt("CurrentLevel", _data.CurrentLevel);
            PlayerPrefs.Save();
        }
    }
}
