﻿using MatchGame.Bindings;
using MatchGame.Views;
using UnityEngine;

namespace MatchGame.Services
{
    public sealed class ApplicationRoot : MonoBehaviour
    {
        private void Awake()
        {
            ViewCollection viewCollection = Binder.GetInstance<ViewCollection>();
            ViewManager viewManager = Binder.GetInstance<ViewManager>();
            viewManager.Open(viewCollection.MainView);
        }
    }
}