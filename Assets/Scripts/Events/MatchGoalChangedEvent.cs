﻿namespace MatchGame.Events
{
    public struct MatchGoalChangedEvent
    {
        public readonly int TileId;
        public readonly int Count;

        public MatchGoalChangedEvent(int tileid, int count)
        {
            TileId = tileid;
            Count = count;
        }
    }
}