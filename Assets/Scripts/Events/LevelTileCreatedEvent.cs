﻿namespace MatchGame.Events
{
    public struct LevelTileCreatedEvent
    {
        public readonly int Index;
        public readonly int TileId;

        public LevelTileCreatedEvent(int index, int tileId)
        {
            Index = index;
            TileId = tileId;
        }
    }
}