﻿namespace MatchGame.Events
{
    public struct MatchGoalCreatedEvent
    {
        public readonly int TileId;
        public readonly int Count;

        public MatchGoalCreatedEvent(int tileid, int count)
        {
            TileId = tileid;
            Count = count;
        }
    }
}