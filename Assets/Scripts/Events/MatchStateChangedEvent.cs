﻿using MatchGame.Controllers;

namespace MatchGame.Events
{
    public struct MatchStateChangedEvent
    {
        public readonly MatchState State;

        public MatchStateChangedEvent(MatchState state)
        {
            State = state;
        }
    }
}