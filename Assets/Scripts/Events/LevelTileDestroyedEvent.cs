﻿namespace MatchGame.Events
{
    public struct LevelTileDestroyedEvent
    {
        public readonly int TileId;
        public readonly int Index;

        public LevelTileDestroyedEvent(int tileId, int index)
        {
            TileId = tileId;
            Index = index;
        }
    }
}