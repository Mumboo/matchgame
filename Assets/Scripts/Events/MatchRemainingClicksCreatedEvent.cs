﻿namespace MatchGame.Events
{
    public struct MatchRemainingClicksCreatedEvent
    {
        public readonly int RemainingClicks;

        public MatchRemainingClicksCreatedEvent(int remainingClicks)
        {
            RemainingClicks = remainingClicks;
        }
    }
}