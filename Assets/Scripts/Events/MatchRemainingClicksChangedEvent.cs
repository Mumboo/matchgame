﻿namespace MatchGame.Events
{
    public struct MatchRemainingClicksChangedEvent
    {
        public readonly int RemainingClicks;

        public MatchRemainingClicksChangedEvent(int remainingClicks)
        {
            RemainingClicks = remainingClicks;
        }
    }
}