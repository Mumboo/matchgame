﻿namespace MatchGame.Events
{
    public struct LevelTileMovedEvent
    {
        public readonly int FromIndex;
        public readonly int ToIndex;

        public LevelTileMovedEvent(int fromIndex, int toIndex)
        {
            FromIndex = fromIndex;
            ToIndex = toIndex;
        }
    }
}