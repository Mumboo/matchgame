﻿namespace MatchGame.Events
{
    public struct LevelTileClickedEvent
    {
        public readonly int Index;

        public LevelTileClickedEvent(int index)
        {
            Index = index;
        }
    }
}