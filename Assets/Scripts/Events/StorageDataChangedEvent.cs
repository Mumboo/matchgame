﻿using MatchGame.Services;

namespace MatchGame.Events
{
    public struct StorageDataChangedEvent
    {
        public readonly StorageData Data;

        public StorageDataChangedEvent(StorageData data)
        {
            Data = data;
        }
    }
}