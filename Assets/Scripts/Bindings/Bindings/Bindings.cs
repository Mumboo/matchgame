﻿using MatchGame.Levels;
using MatchGame.Pools;
using MatchGame.Services;
using MatchGame.Views;
using UnityEngine;

namespace MatchGame.Bindings
{
    public sealed partial class Bindings
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Bind()
        {
            Bindings bindings = Resources.Load<Bindings>("Bindings");
            GameObject binder = new GameObject("Bindings");
            Object.DontDestroyOnLoad(binder);

            Binder.Bind<Storage>(new InstanceBindConfig<Storage>(new Storage()));
            Binder.Bind<Tileset>(new InstanceBindConfig<Tileset>(bindings._tileset));

            BindPrefabPool(binder);
            BindViewManager(binder, bindings);
            BindAudioService(binder, bindings);
        }

        private static void BindPrefabPool(GameObject binder)
        {
            GameObject poolParent = new GameObject("Prefab Pool");
            poolParent.transform.SetParent(binder.transform);

            PrefabPool pool = poolParent.AddComponent<PrefabPool>();
            Binder.Bind<PrefabPool>(new InstanceBindConfig<PrefabPool>(pool));
        }

        private static void BindViewManager(GameObject binder, Bindings bindings)
        {
            ViewManager viewManager = GameObject.Instantiate(bindings._viewManager, binder.transform);
            Binder.Bind<ViewManager>(new InstanceBindConfig<ViewManager>(viewManager));
            Binder.Bind<ViewCollection>(new InstanceBindConfig<ViewCollection>(bindings._viewCollection));
        }

        private static void BindAudioService(GameObject binder, Bindings bindings)
        {
            AudioService audio = Instantiate(bindings._audio, binder.transform);
            Binder.Bind<AudioService>(new InstanceBindConfig<AudioService>(audio));
        }
    }
}