﻿using MatchGame.Levels;
using MatchGame.Services;
using MatchGame.Views;
using UnityEngine;

namespace MatchGame.Bindings
{
    public sealed partial class Bindings : ScriptableObject
    {
        [SerializeField]
        private Tileset _tileset;

        [SerializeField]
        private ViewManager _viewManager;

        [SerializeField]
        private ViewCollection _viewCollection;

        [SerializeField]
        private AudioService _audio;
    }
}