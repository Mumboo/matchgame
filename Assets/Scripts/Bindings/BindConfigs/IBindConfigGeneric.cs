﻿namespace MatchGame.Bindings
{
    public interface IBindConfig<T> : IBindConfig
    {
        new T GetInstance();
    }
}