﻿namespace MatchGame.Bindings
{
    public interface IBindConfig
    {
        object GetInstance();
    }
}