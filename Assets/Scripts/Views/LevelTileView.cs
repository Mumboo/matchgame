﻿using MatchGame.Bindings;
using MatchGame.Events;
using MatchGame.Levels;
using MatchGame.Pools;
using MatchGame.Services;
using UnityEngine;

namespace MatchGame.Views
{
    public sealed class LevelTileView : MonoBehaviour, IPoolable
    {
        [SerializeField]
        private SpriteRenderer _renderer;

        [SerializeField]
        private Animator _animator;

        private int _index;
        private TilesetTile _tile;

        private PrefabPool _pool;
        private AudioService _audio;

        private bool _clicked;

        private void Awake()
        {
            _pool = Binder.GetInstance<PrefabPool>();
            _audio = Binder.GetInstance<AudioService>();
        }

        private void Update()
        {
            if (_clicked)
            {
                _clicked = false;
                _animator.SetTrigger("NoMatches");
            }
        }

        private void OnMouseUpAsButton()
        {
            _clicked = true;
            if (_tile.ClickAudioClip != null)
                _audio.PlaySound(_tile.ClickAudioClip);
            Dispatcher.Dispatch(this, new LevelTileClickedEvent(_index));
        }

        public void Initialize(int index, TilesetTile tile)
        {
            _index = index;
            _tile = tile;
            _renderer.sprite = tile.Image;

            if (_tile.CreateEffect != null)
                _pool.Spawn(_tile.CreateEffect, transform.position, transform.rotation);
            if (_tile.CreateAudioClip != null)
                _audio.PlaySound(_tile.CreateAudioClip);
        }

        public void Destroy()
        {
            if (_tile.DestroyEffect != null)
                _pool.Spawn(_tile.DestroyEffect, transform.position, transform.rotation);
            if (_tile.DestroyAudioClip != null)
                _audio.PlaySound(_tile.DestroyAudioClip);
        }

        public void Move(int index)
        {
            _index = index;
            _animator.SetTrigger("Falling");
        }

        public void OnSpawned()
        {
            _animator.SetTrigger("Reset");
        }

        public void OnDespawned()
        {
            StopAllCoroutines();
        }
    }
}