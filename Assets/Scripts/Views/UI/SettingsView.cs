﻿using MatchGame.Bindings;
using MatchGame.Services;
using UnityEngine;
using UnityEngine.UI;

namespace MatchGame.Views
{
    public sealed class SettingsView : View
    {
        [SerializeField]
        private Toggle _musicToggle;
        [SerializeField]
        private Toggle _soundToggle;

        private ViewManager _viewManager;
        private AudioService _audio;

        private void Awake()
        {
            _viewManager = Binder.GetInstance<ViewManager>();
            _audio = Binder.GetInstance<AudioService>();

            _musicToggle.isOn = !_audio.MusicMute;
            _soundToggle.isOn = !_audio.SoundMute;
        }

        public void Back()
        {
            _viewManager.Close(this);
        }

        public void MuteMusic(bool value)
        {
            _audio.MusicMute = !value;
        }

        public void MuteSound(bool value)
        {
            _audio.SoundMute = !value;
        }
    }
}