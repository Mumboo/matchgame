﻿using MatchGame.Bindings;
using MatchGame.Events;

namespace MatchGame.Views
{
    public sealed class MainView : View
    {
        private ViewCollection _viewCollection;
        private ViewManager _viewManager;

        private void Awake()
        {
            _viewCollection = Binder.GetInstance<ViewCollection>();
            _viewManager = Binder.GetInstance<ViewManager>();
        }

        public void Play()
        {
            View gameView = _viewCollection.GameView;
            _viewManager.Close(this);
            _viewManager.Open(gameView);
            Dispatcher.Dispatch(this, new PlayButtonClickedEvent());
        }

        public void Settings()
        {
            View settingsView = _viewCollection.SettingsView;
            _viewManager.Hide(this);
            _viewManager.Open(settingsView);
        }
    }
}