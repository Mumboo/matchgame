﻿using MatchGame.Bindings;
using MatchGame.Services;
using UnityEngine;

namespace MatchGame.Views
{
    public sealed class PlayAudio : MonoBehaviour
    {
        [SerializeField]
        private AudioClip _clip;

        [SerializeField]
        private bool _playOnEnable;

        private AudioService _audio;

        private void Awake()
        {
            _audio = Binder.GetInstance<AudioService>();
        }

        private void OnEnable()
        {
            if (_playOnEnable)
                Play();
        }

        public void Play()
        {
            _audio.PlaySound(_clip);
        }
    }
}