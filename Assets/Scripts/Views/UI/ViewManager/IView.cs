﻿namespace MatchGame.Views
{
    public interface IView
    {
        bool Opened { get; }

        void Open();
        void Close();
        void Hide();
    }
}