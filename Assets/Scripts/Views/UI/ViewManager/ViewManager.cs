﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace MatchGame.Views
{
    public sealed class ViewManager : MonoBehaviour
    {
        public IView Current { get { return _views.Count > 0 ? _views[_views.Count - 1] : null; } }

        public ReadOnlyCollection<IView> All { get { return _readonlyViews; } }

        [SerializeField]
        private Transform _container;

        private readonly List<IView> _views;
        private readonly ReadOnlyCollection<IView> _readonlyViews;

        public ViewManager()
        {
            _views = new List<IView>();
            _readonlyViews = _views.AsReadOnly();
        }

        public void Open(IView view)
        {
            view = BuildView(view);
            _views.Add(view);
            view.Open();
        }

        public void CloseAll()
        {
            while (_views.Count > 0)
                Close();
        }

        public void Close()
        {
            if (_views.Count > 0)
                Close(_views[_views.Count - 1]);
        }

        public void Close(IView view)
        {
            int viewIndex = _views.IndexOf(view);
            if (viewIndex != -1)
            {
                view.Close();
                _views.RemoveAt(viewIndex);
                DestroyView(view);

                SetViewActive(Current, true);
            }
        }

        public void Hide(IView view)
        {
            if (_views.Contains(view))
            {
                view.Hide();
                SetViewActive(view, false);
            }
        }

        private IView BuildView(IView view)
        {
            IViewObject viewObject = view as IViewObject;
            if (viewObject != null)
            {
                GameObject viewInstance = Object.Instantiate(viewObject.Object, _container);
                return viewInstance.GetComponent<IView>();
            }
            return view;
        }

        private void DestroyView(IView view)
        {
            IViewObject viewObject = view as IViewObject;
            if (viewObject != null)
                Destroy(viewObject.Object);
        }

        private void SetViewActive(IView view, bool active)
        {
            IViewObject viewObject = view as IViewObject;
            if (viewObject != null)
                viewObject.Object.SetActive(active);
        }
    }
}