﻿using UnityEngine;

namespace MatchGame.Views
{
    [CreateAssetMenu(menuName = "Views/View Collection", fileName = "View Collection")]
    public sealed class ViewCollection : ScriptableObject
    {
        [SerializeField]
        private View _mainView;
        public View MainView { get { return _mainView; } }

        [SerializeField]
        private View _settingsView;
        public View SettingsView { get { return _settingsView; } }

        [SerializeField]
        private View _gameView;
        public View GameView { get { return _gameView; } }
    }
}