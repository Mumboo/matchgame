﻿using UnityEngine;

namespace MatchGame.Views
{
    public abstract class View : MonoBehaviour, IViewObject
    {
        private bool _opened;
        public bool Opened { get { return _opened; } }

        public GameObject Object { get { return gameObject; } }

        public virtual void Open()
        {
            _opened = true;
        }

        public virtual void Close()
        {
            _opened = false;
        }

        public virtual void Hide() { }
    }
}