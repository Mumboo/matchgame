﻿using UnityEngine;

namespace MatchGame.Views
{
    public interface IViewObject : IView
    {
        GameObject Object { get; }
    }
}