﻿using MatchGame.Bindings;
using MatchGame.Events;
using MatchGame.Levels;
using UnityEngine;
using UnityEngine.UI;

namespace MatchGame.Views
{
    public sealed class GoalView : MonoBehaviour
    {
        [SerializeField]
        private Image _image;

        [SerializeField]
        private Text _text;

        private Tileset _tileset;
        private int _tileId = -1;
        private int _count;
        private int _maxCount;

        private void Awake()
        {
            _tileset = Binder.GetInstance<Tileset>();
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<MatchGoalChangedEvent>(OnMatchGoalChanged);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<MatchGoalChangedEvent>(OnMatchGoalChanged);
        }

        public void Initialize(int tileId, int count)
        {
            _tileId = tileId;
            _count = count;
            _maxCount = count;
            _image.sprite = _tileset.GetTile(tileId).Image;
            UpdateText();
        }

        private void OnMatchGoalChanged(object sender, MatchGoalChangedEvent args)
        {
            if (_tileId == args.TileId)
            {
                _count = args.Count;
                UpdateText();
            }
        }

        private void UpdateText()
        {
            _text.text = string.Format("{0}/{1}", _maxCount - _count, _maxCount);
        }
    }
}