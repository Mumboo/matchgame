﻿using System.Collections.Generic;
using MatchGame.Bindings;
using MatchGame.Controllers;
using MatchGame.Events;
using MatchGame.Services;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MatchGame.Views
{
    public sealed class GameView : View
    {
        [SerializeField]
        private GameObject _victoryView;
        [SerializeField]
        private GameObject _defeatView;

        [SerializeField]
        private Image _progressImage;

        [SerializeField]
        private Transform _goalsContainer;
        [SerializeField]
        private GoalView _goalPrefab;

        [SerializeField]
        private Text _remainingClicksText;

        private Storage _storage;
        private ViewManager _viewManager;

        private readonly Dictionary<int, int> _pendingTiles;
        private int _totalCount;

        private int _currentRemainingClicks;
        private int _totalRemainingClicks;

        public GameView()
        {
            _pendingTiles = new Dictionary<int, int>();
        }

        private void Awake()
        {
            _storage = Binder.GetInstance<Storage>();
            _viewManager = Binder.GetInstance<ViewManager>();
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<MatchStateChangedEvent>(OnMatchStateChanged);
            Dispatcher.Subscribe<MatchGoalCreatedEvent>(OnMatchGoalCreated);
            Dispatcher.Subscribe<MatchGoalChangedEvent>(OnMatchGoalChanged);
            Dispatcher.Subscribe<MatchRemainingClicksCreatedEvent>(OnMatchRemainingClicksCreated);
            Dispatcher.Subscribe<MatchRemainingClicksChangedEvent>(OnMatchRemainingClicksChanged);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<MatchStateChangedEvent>(OnMatchStateChanged);
            Dispatcher.Unsubscribe<MatchGoalCreatedEvent>(OnMatchGoalCreated);
            Dispatcher.Unsubscribe<MatchGoalChangedEvent>(OnMatchGoalChanged);
            Dispatcher.Unsubscribe<MatchRemainingClicksCreatedEvent>(OnMatchRemainingClicksCreated);
            Dispatcher.Unsubscribe<MatchRemainingClicksChangedEvent>(OnMatchRemainingClicksChanged);
        }

        public void NextLevel()
        {
            StorageData data = _storage.Data;
            data.CurrentLevel++;
            _storage.Data = data;
            _storage.Save();
            _viewManager.Close(this);
            SceneManager.LoadScene("Game");
        }

        public void Restart()
        {
            _viewManager.Close(this);
            SceneManager.LoadScene("Game");
        }

        private void OnMatchStateChanged(object sender, MatchStateChangedEvent args)
        {
            _victoryView.SetActive(args.State == MatchState.Victory);
            _defeatView.SetActive(args.State == MatchState.Defeat);
        }

        private void OnMatchGoalCreated(object sender, MatchGoalCreatedEvent args)
        {
            _pendingTiles.Add(args.TileId, args.Count);
            _totalCount += args.Count;

            GoalView goal = Instantiate(_goalPrefab, _goalsContainer);
            goal.Initialize(args.TileId, args.Count);
        }

        private void OnMatchGoalChanged(object sender, MatchGoalChangedEvent args)
        {
            _pendingTiles[args.TileId] = args.Count;
            UpdateProgress();
        }

        private void OnMatchRemainingClicksCreated(object sender, MatchRemainingClicksCreatedEvent args)
        {
            _currentRemainingClicks = args.RemainingClicks;
            _totalRemainingClicks = args.RemainingClicks;
            UpdateRemainingClicksText();
        }

        private void OnMatchRemainingClicksChanged(object sender, MatchRemainingClicksChangedEvent args)
        {
            _currentRemainingClicks = args.RemainingClicks;
            UpdateRemainingClicksText();
        }

        private void UpdateRemainingClicksText()
        {
            _remainingClicksText.text = string.Format("{0}/{1}", _currentRemainingClicks, _totalRemainingClicks);
        }

        private void UpdateProgress()
        {
            int totalCount = 0;
            foreach (int count in _pendingTiles.Values)
                totalCount += count;
            _progressImage.fillAmount = 1f - totalCount / (float)_totalCount;
        }
    }
}