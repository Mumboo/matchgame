﻿using MatchGame.Bindings;
using MatchGame.Pools;
using UnityEngine;

namespace MatchGame.Views
{
    public sealed class PoolableParticleSystem : MonoBehaviour, IPoolable
    {
        [SerializeField]
        private ParticleSystem[] _particles;

        private PrefabPool _pool;

        private void Awake()
        {
            _pool = Binder.GetInstance<PrefabPool>();
        }

        private void Update()
        {
            bool alive = false;
            for (int i = 0; i < _particles.Length; i++)
            {
                if (_particles[i].IsAlive(true))
                {
                    alive = true;
                    break;
                }
            }

            if (!alive)
                _pool.Despawn(gameObject);
        }

        public void OnSpawned()
        {
            for (int i = 0; i < _particles.Length; i++)
                _particles[i].Play();
        }

        public void OnDespawned()
        {
            for (int i = 0; i < _particles.Length; i++)
                _particles[i].Stop(true);
        }
    }
}