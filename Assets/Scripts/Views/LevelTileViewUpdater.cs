﻿using System.Collections;
using System.Collections.Generic;
using MatchGame.Bindings;
using MatchGame.Events;
using MatchGame.Levels;
using MatchGame.Models;
using MatchGame.Pools;
using UnityEngine;

namespace MatchGame.Views
{
    public sealed class LevelTileViewUpdater : MonoBehaviour
    {
        [SerializeField]
        private float _tileSize = 1;

        [SerializeField]
        private float _spawnHeight = 3;
        [SerializeField]
        private float _moveSpeed = 5;
        [SerializeField]
        private float _moveAcceleration = 10;

        [SerializeField]
        private LevelTileView _prefab;

        private readonly Dictionary<int, LevelTileView> _views;
        private PrefabPool _pool;
        private Tileset _tileset;

        public LevelTileViewUpdater()
        {
            _views = new Dictionary<int, LevelTileView>();
        }

        private void Awake()
        {
            _pool = Binder.GetInstance<PrefabPool>();
            _tileset = Binder.GetInstance<Tileset>();
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<LevelTileCreatedEvent>(OnLevelTileCreated);
            Dispatcher.Subscribe<LevelTileDestroyedEvent>(OnLevelTileDestroyed);
            Dispatcher.Subscribe<LevelTileMovedEvent>(OnLevelTileMoved);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<LevelTileCreatedEvent>(OnLevelTileCreated);
            Dispatcher.Unsubscribe<LevelTileDestroyedEvent>(OnLevelTileDestroyed);
            Dispatcher.Unsubscribe<LevelTileMovedEvent>(OnLevelTileMoved);
        }

        private void OnLevelTileCreated(object sender, LevelTileCreatedEvent args)
        {
            LevelModel model = Binder.GetInstance<LevelModel>();

            Vector3 position = (Vector2)model.IndexToPosition(args.Index) * _tileSize;
            GameObject instance = _pool.Spawn(_prefab.gameObject, position, Quaternion.identity, transform);
            if (args.TileId < _tileset.Count)
                instance.transform.position = instance.transform.position + Vector3.up * _spawnHeight;

            LevelTileView viewInstance = instance.GetComponent<LevelTileView>();
            viewInstance.Initialize(args.Index, _tileset.GetTile(args.TileId));
            if (args.TileId < _tileset.Count)
                viewInstance.StartCoroutine(MoveTile(viewInstance, args.Index, position));

            _views[args.Index] = viewInstance;
        }

        private void OnLevelTileDestroyed(object sender, LevelTileDestroyedEvent args)
        {
            LevelTileView view;
            if (_views.TryGetValue(args.Index, out view))
            {
                view.Destroy();
                _views.Remove(args.Index);
                _pool.Despawn(view.gameObject);
            }
        }

        private void OnLevelTileMoved(object sender, LevelTileMovedEvent args)
        {
            LevelModel model = Binder.GetInstance<LevelModel>();

            LevelTileView view;
            if (_views.TryGetValue(args.FromIndex, out view))
            {
                view.StopAllCoroutines();
                view.StartCoroutine(MoveTile(view, args.ToIndex, (Vector2)model.IndexToPosition(args.ToIndex) * _tileSize));
                _views[args.ToIndex] = view;
                _views.Remove(args.FromIndex);
            }
        }

        private IEnumerator MoveTile(LevelTileView view, int index, Vector3 position)
        {
            float speed = _moveSpeed;
            while (view.transform.position != position)
            {
                view.transform.position = Vector3.MoveTowards(view.transform.position, position, Time.deltaTime * speed);
                speed += Time.deltaTime * _moveAcceleration;
                yield return null;
            }
            view.Move(index);
        }
    }
}