﻿using System.Collections.Generic;
using MatchGame.Collections;
using MatchGame.Levels;
using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Controllers
{
    public sealed class RewardTileMatchingStrategy : ITileMatchingStrategy
    {
        public void Process(LevelModel model, Tileset tileset, int tileId, ReadOnlySet<Vector2Int> positions)
        {
            if (positions.Count >= 4)
            {
                Vector2Int initialPosition = GetInitialPosition(positions);
                int index = model.PositionToIndex(initialPosition);
                int rewardTileId;

                if (ContainsHorizontal(initialPosition, positions))
                    rewardTileId = tileset.GetRewardTileId<HorizontalLineDestroyTilesetTile>();
                else if (ContainsVertical(initialPosition, positions))
                    rewardTileId = tileset.GetRewardTileId<VerticalLineDestroyTilesetTile>();
                else
                    rewardTileId = tileset.GetRewardTileId<SquareDestroyTilesetTile>();

                model.CreateTile(index, rewardTileId);
            }
        }

        private bool ContainsHorizontal(Vector2Int initial, ReadOnlySet<Vector2Int> positions)
        {
            for (int i = 0; i < 4; i++)
            {
                Vector2Int position = new Vector2Int(initial.x - i, initial.y);
                if (!positions.Contains(position))
                    break;
            }

            for (int i = 0; i < 4; i++)
            {
                Vector2Int position = new Vector2Int(initial.x + i, initial.y);
                if (!positions.Contains(position))
                    return false;
            }
            return true;
        }

        private bool ContainsVertical(Vector2Int initial, ReadOnlySet<Vector2Int> positions)
        {
            for (int i = 0; i < 4; i++)
            {
                Vector2Int position = new Vector2Int(initial.x, initial.y - i);
                if (!positions.Contains(position))
                    break;
            }

            for (int i = 0; i < 4; i++)
            {
                Vector2Int position = new Vector2Int(initial.x, initial.y + i);
                if (!positions.Contains(position))
                    return false;
            }
            return true;
        }

        private Vector2Int GetInitialPosition(ReadOnlySet<Vector2Int> positions)
        {
            IEnumerator<Vector2Int> enumerator = positions.GetEnumerator();
            enumerator.MoveNext();
            return enumerator.Current;
        }
    }
}
