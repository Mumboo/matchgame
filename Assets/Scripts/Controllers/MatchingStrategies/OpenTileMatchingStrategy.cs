﻿using MatchGame.Collections;
using MatchGame.Levels;
using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Controllers
{
    public sealed class OpenTileMatchingStrategy : ITileMatchingStrategy
    {
        public void Process(LevelModel model, Tileset tileset, int tileId, ReadOnlySet<Vector2Int> positions)
        {
            TilesetTile tile = tileset.GetTile(tileId);
            foreach (Vector2Int position in positions)
                tile.Open(model, position);
        }
    }
}
