﻿using MatchGame.Collections;
using MatchGame.Levels;
using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Controllers
{
    public interface ITileMatchingStrategy
    {
        void Process(LevelModel model, Tileset tileset, int tileId, ReadOnlySet<Vector2Int> positions);
    }
}
