﻿using MatchGame.Collections;
using MatchGame.Levels;
using MatchGame.Models;
using UnityEngine;

namespace MatchGame.Controllers
{
    public sealed class DestroyTileMatchingStrategy : ITileMatchingStrategy
    {
        public void Process(LevelModel model, Tileset tileset, int tileId, ReadOnlySet<Vector2Int> positions)
        {
            if (positions.Count > 1)
            {
                foreach (Vector2Int position in positions)
                {
                    int index = model.PositionToIndex(position);
                    model.DestroyTile(index);
                }
            }
        }
    }
}
