﻿using System.Collections.Generic;
using MatchGame.Bindings;
using MatchGame.Events;
using MatchGame.Levels;
using MatchGame.Services;
using UnityEngine;
using Random = System.Random;

namespace MatchGame.Controllers
{
    public sealed class MatchController : MonoBehaviour
    {
        private MatchState _state;
        public MatchState State
        {
            get { return _state; }
            private set
            {
                if (_state != value)
                {
                    _state = value;
                    Dispatcher.Dispatch(this, new MatchStateChangedEvent(_state));
                }
            }
        }

        private Tileset _tileset;
        private Storage _storage;

        private int _remainingClicks;
        private readonly Dictionary<int, int> _goals;

        public MatchController()
        {
            _goals = new Dictionary<int, int>();
        }

        private void Awake()
        {
            _tileset = Binder.GetInstance<Tileset>();
            _storage = Binder.GetInstance<Storage>();
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<PlayButtonClickedEvent>(OnPlayButtonClicked);
            Dispatcher.Subscribe<LevelTileDestroyedEvent>(OnLevelTileDestroyed);
            Dispatcher.Subscribe<LevelTileClickedEvent>(OnLevelTileClicked);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<PlayButtonClickedEvent>(OnPlayButtonClicked);
            Dispatcher.Unsubscribe<LevelTileDestroyedEvent>(OnLevelTileDestroyed);
            Dispatcher.Unsubscribe<LevelTileClickedEvent>(OnLevelTileClicked);
        }

        private void OnPlayButtonClicked(object sender, PlayButtonClickedEvent args)
        {
            State = MatchState.InProgress;

            int currentLevel = _storage.Data.CurrentLevel;
            Random random = new Random(currentLevel);
            int tiles = Mathf.Max(currentLevel % _tileset.Count, 2);
            for (int i = 0; i < tiles; i++)
            {
                int tileId = random.Next(_tileset.Count);
                while (_goals.ContainsKey(tileId))
                    tileId = random.Next(_tileset.Count);

                int count = Mathf.Max(currentLevel / tiles, 5);
                _goals.Add(tileId, count);
                Dispatcher.Dispatch(this, new MatchGoalCreatedEvent(tileId, count));
            }

            _remainingClicks = 30;
            Dispatcher.Dispatch(this, new MatchRemainingClicksCreatedEvent(_remainingClicks));
        }

        private void OnLevelTileDestroyed(object sender, LevelTileDestroyedEvent args)
        {
            int count;
            if (_goals.TryGetValue(args.TileId, out count))
            {
                count = Mathf.Max(count - 1, 0);
                _goals[args.TileId] = count;

                Dispatcher.Dispatch(this, new MatchGoalChangedEvent(args.TileId, count));
                if (count == 0)
                    CheckCompletition();
            }
        }

        private void OnLevelTileClicked(object sender, LevelTileClickedEvent args)
        {
            _remainingClicks = Mathf.Max(_remainingClicks - 1, 0);
            Dispatcher.Dispatch(this, new MatchRemainingClicksChangedEvent(_remainingClicks));
            CheckCompletition();
        }

        private void CheckCompletition()
        {
            if (State == MatchState.InProgress)
            {
                if (_remainingClicks == 0)
                {
                    State = MatchState.Defeat;
                }
                else
                {

                    bool cleared = true;
                    foreach (KeyValuePair<int, int> pair in _goals)
                    {
                        if (pair.Value > 0)
                        {
                            cleared = false;
                            break;
                        }
                    }

                    if (cleared)
                    {
                        State = MatchState.Victory;
                    }
                }
            }
        }
    }
}