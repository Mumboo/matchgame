﻿namespace MatchGame.Controllers
{
    public enum MatchState
    {
        None,
        InProgress,
        Victory,
        Defeat
    }
}