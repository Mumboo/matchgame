﻿using System.Collections.Generic;
using MatchGame.Bindings;
using MatchGame.Collections;
using MatchGame.Events;
using MatchGame.Levels;
using MatchGame.Models;
using MatchGame.Services;
using UnityEngine;

namespace MatchGame.Controllers
{
    public sealed class GameController : MonoBehaviour
    {
        [SerializeField]
        private int _width;
        [SerializeField]
        private int _height;

        private readonly HashSet<Vector2Int> _matchedPositions;
        private readonly ReadOnlySet<Vector2Int> _readonlyMatchedPositions;
        private readonly HashSet<int> _visitedIndices;

        private Storage _storage;
        private LevelModel _model;
        private Tileset _tileset;
        private LevelBuilder _builder;

        private readonly ITileMatchingStrategy[] _strategies;

        public GameController()
        {
            _matchedPositions = new HashSet<Vector2Int>();
            _readonlyMatchedPositions = new ReadOnlySet<Vector2Int>(_matchedPositions);
            _visitedIndices = new HashSet<int>();
            _strategies = new ITileMatchingStrategy[]
            {
                new DestroyTileMatchingStrategy(),
                new OpenTileMatchingStrategy(),
                new RewardTileMatchingStrategy()
            };
        }

        private void Awake()
        {
            _storage = Binder.GetInstance<Storage>();
            _tileset = Binder.GetInstance<Tileset>();
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<MatchStateChangedEvent>(OnMatchStateChangedEvent);
            Dispatcher.Subscribe<LevelTileClickedEvent>(OnLevelTileClicked);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<MatchStateChangedEvent>(OnMatchStateChangedEvent);
            Dispatcher.Unsubscribe<LevelTileClickedEvent>(OnLevelTileClicked);
        }

        private void OnDestroy()
        {
            Binder.Unbind<LevelModel>();
        }

        private void OnMatchStateChangedEvent(object sender, MatchStateChangedEvent args)
        {
            if (args.State == MatchState.InProgress)
            {
                _model = new LevelModel(_width, _height);
                Binder.Bind<LevelModel>(new InstanceBindConfig<LevelModel>(_model));

                _builder = new LevelBuilder(_model, _tileset, _storage.Data.CurrentLevel);
                _builder.Build();
            }
        }

        private void OnLevelTileClicked(object sender, LevelTileClickedEvent args)
        {
            LevelCell initialCell = _model.GetCell(args.Index);
            Vector2Int position = _model.IndexToPosition(args.Index);

            PopulateMatchedIndices(position.x, position.y, initialCell.TileId);
            if (_matchedPositions.Count > 0)
            {
                for (int i = 0; i < _strategies.Length; i++)
                    _strategies[i].Process(_model, _tileset, initialCell.TileId, _readonlyMatchedPositions);
                _matchedPositions.Clear();
                _visitedIndices.Clear();

                for (int x = 0; x < _width; x++)
                    ApplyGravity(x);
                _builder.Build();
            }
        }

        private void ApplyGravity(int x)
        {
            for (int y = 0; y < _height; y++)
            {
                int currentCellIndex = _model.PositionToIndex(x, y);
                LevelCell currentCell = _model.GetCell(currentCellIndex);
                if (currentCell == LevelCell.Empty)
                {
                    for (int nextY = y + 1; nextY < _height; nextY++)
                    {
                        int nextCellIndex = _model.PositionToIndex(x, nextY);
                        LevelCell nextCell = _model.GetCell(nextCellIndex);
                        if (nextCell != LevelCell.Empty)
                        {
                            _model.MoveTile(nextCellIndex, currentCellIndex);
                            break;
                        }
                    }
                }
            }
        }

        private void PopulateMatchedIndices(int x, int y, int id)
        {
            if (x < 0 || x >= _width || y < 0 || y >= _height)
                return;

            int index = _model.PositionToIndex(x, y);
            if (!_visitedIndices.Contains(index))
            {
                _visitedIndices.Add(index);
                LevelCell cell = _model.GetCell(index);
                if (cell.TileId == id)
                {
                    _matchedPositions.Add(new Vector2Int(x, y));
                    PopulateMatchedIndices(x - 1, y, id);
                    PopulateMatchedIndices(x + 1, y, id);
                    PopulateMatchedIndices(x, y - 1, id);
                    PopulateMatchedIndices(x, y + 1, id);
                }
            }
        }
    }
}